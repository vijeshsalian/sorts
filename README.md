# README #


### Sorts ###

* This repository has different sorting algorithms in C#. This can be used as a utility. This also serves as educational to learn sorting algorithm. This will be continually updated to include different algorithms and will also be optimized.

### How do I get set up? ###

* Get Visual Studio Free
* Download source
* Play 

### Contribution guidelines ###

* IMPORTANT RULE: Check-in better code than you check-out. 
* Please feel free to add newer algorithms.
* Note that these algorithms need to used as an API. Usage of this API needs to be easy.

### Contact ###

* Repo owner or admin