﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomSort
{
    public static class Extension
    {
        public static IList<T> MergeSort<T>(this IList<T> input) where T:IComparable<T>
        {
            ISorter<T> sorter = new MergeSorter<T>();
            sorter.SortIt(ref input);
            return input;
        }
    }
}
