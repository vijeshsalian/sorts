﻿using System;
using System.Collections.Generic;

namespace CustomSort
{
    public class MergeSorter<T> : AbstractSorter<T> where T : IComparable<T>
    {
        IList<T> tempList = new List<T>();
        public override void SortIt(ref IList<T> input)
        {
            int right = input.Count - 1;
            int left = 0;
            
            Setup(ref input);

            MergeSort(left, right);

            input = this.input;
        }

        protected override void Setup(ref IList<T> input)
        {
            base.Setup(ref input);

            foreach (T item in input)
            {
                tempList.Add(item);
            }
        }

        protected void Merge(int left, int mid, int right)
        {
            int sortedListIndex = left;
            int leftListIndex = left;
            int rightListIndex = mid+1;

            while (leftListIndex <= mid && rightListIndex <=right)
            {
                if (input[leftListIndex].CompareTo(input[rightListIndex]) <= 0)
                {
                    tempList[sortedListIndex++] = input[leftListIndex++];
                }
                else
                {
                    tempList[sortedListIndex++] = input[rightListIndex++];
                }
            }

            while (leftListIndex <= mid)
            {
                tempList[sortedListIndex++] = input[leftListIndex++];
            }

            input.Clear();
            foreach (T item in tempList)
            {
                input.Add(item);
            }
        }
        
        protected void MergeSort(int left, int right)
        {
            if (left < right)
            {
                int mid = (left + right) / 2;
                MergeSort(left, mid);
                MergeSort(mid + 1, right);
                Merge(left, mid, right);
            }
        }
    }
}
