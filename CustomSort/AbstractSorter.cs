﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomSort
{
    public abstract class AbstractSorter<T> : ISorter<T> where T:IComparable<T>
    {
        public IList<T> input;
        public abstract void SortIt(ref IList<T> input);

        protected virtual void Setup(ref IList<T> input)
        {
            this.input = input.ToList<T>();           
            
        }

    }
}
