﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CustomSort;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Runtime.Remoting.Messaging;

namespace Test
{
    [TestClass]
    public class Tester
    {
        [TestMethod]
        public void MergeSortForInts()
        {
            Console.WriteLine("--Start: Running Merge sort for integers");
            IList<int> numbers = new List<int>() { 1, 4, 2, 6, 2, 4, 98, 65, 43, 45, 67 };
            int countBeforeSort = numbers.Count;
            
            Stopwatch watch = new Stopwatch();
            watch.Start();
            IList<int> newNumbers = numbers.MergeSort();
            watch.Stop();            
            Console.WriteLine($"Time Taken is {watch.Elapsed.TotalMilliseconds}");
            Assert.IsTrue(countBeforeSort == numbers.Count);

            int prevNum = int.MinValue;
            foreach(int num in newNumbers)
            {
                Assert.IsTrue(prevNum <= num);
                prevNum = num;                    
            }
            Console.WriteLine("--Fin.");
        }

        [TestMethod]
        public void MergeSortForEmployees()
        {
            Console.WriteLine("--Start: Running Merge sort for Employees");
            IList<Employee> employees = new List<Employee>
            {
                new Employee() {EmpId = 1, Name = "Jon", Salary = 112345},
                new Employee() {EmpId = 2, Name = "Sansa", Salary = 92345},
                new Employee() {EmpId = 3, Name = "Theon", Salary = 83456},
                new Employee() {EmpId = 4, Name = "Bran", Salary = 44567},
                new Employee() {EmpId = 5, Name = "Arya", Salary = 55778}
            };

            int countBeforeSort = employees.Count;
            Stopwatch watch = new Stopwatch();
            watch.Start();
            IList<Employee> newEmployees = employees.MergeSort();
            watch.Stop();

            (employees as List<Employee>).Sort((a, b) => a.Name.CompareTo(b.Name));
            
            Console.WriteLine($"Time Taken is {watch.Elapsed.TotalMilliseconds}");

            Assert.IsTrue(countBeforeSort == newEmployees.Count);
            Assert.IsTrue(employees.SequenceEqual(newEmployees));
            
            Console.WriteLine("--Fin.");

        }
        
        public void SortEmployee()
        {
            List<Employee> employees = new List<Employee>
            {
                new Employee() {EmpId = 1, Name = "Jon", Salary = 112345},
                new Employee() {EmpId = 2, Name = "Sansa", Salary = 92345},
                new Employee() {EmpId = 3, Name = "Theon", Salary = 83456},
                new Employee() {EmpId = 4, Name = "Bran", Salary = 44567},
                new Employee() {EmpId = 5, Name = "Arya", Salary = 55778}
            };
            
            employees.Sort((a,b)=> a.Name.CompareTo(b.Name));

            foreach (Employee emp in employees)
            {
                Console.WriteLine(emp.Name);
            }
        }
    }
    
    public class Employee: IComparable<Employee>
    {
        public string Name { get; set; }
        public int EmpId { get; set; }
        public int Salary { get; set; }
        
        public int CompareTo(Employee other)
        {
            return string.Compare(Name, other.Name);
        }
    }

    public class SortBySalary : IComparer<Employee>
    {
        public int Compare(Employee x, Employee y)
        {
            if (x.Salary > y.Salary)
                return -1;
            return x.Salary == y.Salary ? 0 : 1;
        }
    }
}
