﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Test;
namespace TestRunner
{
    class Program
    {
        static void Main(string[] args)
        {
            RunTests();
            Console.ReadKey();
        }

        static void RunTests()
        {
            Tester t = new Tester();

            t.MergeSortForInts();
            t.MergeSortForEmployees();
        }
    }
}
